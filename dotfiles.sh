#!/usr/bin/env bash

dotfile_directory=$PWD

my_dotfiles=(
	$(find . -type f | grep -v dotfiles.sh | sed 's/^.\///')
)

my_dotdirs=(
	$(find . -type d | grep -v ^.$ | sed 's/^.\///')
)

for i in ${my_dotdirs[@]}; do
	[[ -d $HOME/$i ]] || mkdir $HOME/$i
done

for i in ${my_dotfiles[@]}; do
	if [[ -L $HOME/$i ]]; then
		diff <(readlink $HOME/$i) <(echo $dotfile_directory/$i) ||
			mv $HOME/$i $HOME/${i}.link$(date +%F_%Hh%Mm%S)
	elif [[ -f $HOME/$i ]]; then
		mv $HOME/$i $HOME/${i}.orig$(date +%F_%Hh%Mm%S)
	fi
	ln -s $dotfile_directory/$i $HOME/$i >/dev/null 2>&1
done
