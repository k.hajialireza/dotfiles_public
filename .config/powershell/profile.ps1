#!/usr/bin/env pwsh

# DEPENDENCIES:
#   - busybox.exe
#   - eza.exe
#   - rg.exe
#   - bat.exe
#   - nvim.exe
#   Install-Module -Name PSFzf -Scope CurrentUser -Force

Set-PSReadLineOption -EditMode Emacs

# --- aliases ---
Set-Alias -Name '..' -Value 'cd ..'
Set-Alias -Name sh -Value "busybox sh"
Remove-Item Alias:ls ; Set-Alias -Name ls -Value eza
#Set-Alias -Name ll -Value 'ls -l'
#Set-Alias -Name ll -Value 'ls -alF'
#Set-Alias -Name ll -Value 'ls -AF'
#Set-Alias -Name la -Value 'ls -A'
#Set-Alias -Name l -Value 'ls -CF'
#Set-Alias -Name less -Value "busybox less" # <- funktioniert so bat?
Set-Alias -Name grep -Value rg
Remove-Item Alias:cat ; Set-Alias -Name cat -Value bat
Set-Alias -Name sort -Value "busybox sort"
Set-Alias -Name uniq -Value "busybox uniq"
Set-Alias -Name head -Value "gc -Head 10"
Set-Alias -Name tail -Value "gc -Tail 10"
Set-Alias -Name sed -Value "busybox sed"
Set-Alias -Name awk -Value "busybox awk"
Set-Alias -Name tar -Value "busybox tar"
Remove-Item Alias:curl
Set-Alias -Name datum -Value "busybox date +%F_%Hh%Mm%S"
Set-Alias -Name vi -Value nvim
Set-Alias -Name vim -Value nvim
function cht {
    param($arg)
    curl.exe https://www.cheat.sh/$arg
}
function cheat {
    param($arg)
    curl.exe https://www.cheat.sh/$arg
}
function which ($command) {
    Get-Command -Name $command -ErrorAction SilentlyContinue | 
    Select-Object -ExpandProperty Path -ErrorAction SilentlyContinue 
} 

# --- fzf ---
Import-Module PSFzf
Set-PsFzfOption -PSReadlineChordProvider 'Ctrl+f' -PSReadlineChordReverseHistory 'Ctrl+r'

# more ideas:
#   https://hamidmosalla.com/2022/12/26/how-to-customize-windows-terminal-and-powershell-using-fzf-neovim-and-beautify-it-with-oh-my-posh/
#   https://github.com/mikemaccana/powershell-profile
#   https://github.com/gerardog/gsudo
#   https://mathieubuisson.github.io/powershell-linux-bash/
#   https://stackoverflow.com/questions/8264655/how-to-make-powershell-tab-completion-work-like-bash
