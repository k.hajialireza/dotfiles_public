vim.g.vimwiki_list = {
  {
    ['path'] = '~/vimwiki/',
    ['syntax'] = 'markdown',
    ['ext'] = '.md.asc',
    ['auto_diary_index'] = 1,
    ['auto_tags'] = 1}
}
vim.g.vimwiki_folding = 'custom' -- ka was hier das beste ist
--"let g:vimwiki_log_level = 'debug'

if vim.fn.has("unix") == 1 and not vim.fn.isdirectory("/data/data/com.termux/") then
	vim.g.taskwiki_markup_syntax = 'markdown'
end

if vim.fn.has("win64") == 1 or vim.fn.has("win32") == 1 then
	vim.api.nvim_set_keymap('n', '<Leader>t', '<Plug>VimwikiToggleListItem', {})
end
--vim.cmd([[
--if has("win64") || has("win32")
--	nnoremap <Leader>t <Plug>VimwikiToggleListItem
--endif
--]])

--  " Use Vim to open external files with the 'vfile:' scheme.  E.g.:
--  "   1) [[vfile:~/Code/PythonProject/abc123.py]]
--  "   2) [[vfile:./|Wiki Home]]
vim.cmd([[
function! VimwikiLinkHandler(link)
  let link = a:link
  if link =~# '^vfile:'
    let link = link[1:]
  else
    return 0
  endif
  let link_infos = vimwiki#base#resolve_link(link)
  if link_infos.filename == ''
    echomsg 'Vimwiki Error: Unable to resolve link!'
    return 0
  else
    exe 'tabnew ' . fnameescape(link_infos.filename)
    return 1
  endif
endfunction
]])
