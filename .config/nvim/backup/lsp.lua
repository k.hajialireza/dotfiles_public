local lsp = require('lsp-zero').preset({})

lsp.ensure_installed({
    'clangd',
    'pyright',
    'lua_ls',
    --'prolog_ls',
    'arduino_language_server',
    'bashls',
    'powershell_es',
    'jsonls',
    'yamlls',
    'ansiblels'
})


lsp.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
  lsp.default_keymaps({buffer = bufnr})
end)

-- (Optional) Configure lua language server for neovim
require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls())

require('lspconfig').arduino_language_server.setup{
    cmd = {
        "arduino-language-server",
        "-cli-config", "~/.arduino15/arduino-cli.yaml",
        "-fqbn", "arduino:avr:uno",
    }
}
lsp.setup()

