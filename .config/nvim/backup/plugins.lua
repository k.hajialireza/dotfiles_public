-- based on 2023-08-19 https://github.com/wbthomason/packer.nvim

-- ABHAENGIGKEITEN!!! <- wie kann lua das Betriebssystem unabhängig einsehen? Path variable?
-- ripgrep
-- task
-- fzf?

-- vimplug equivalent? mit if für wenn nur vim drauf 

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Example using a list of specs with the default options
vim.g.mapleader = " " -- Make sure to set `mapleader` before lazy so your mappings are correct

require("lazy").setup({
  'jamessan/vim-gnupg',
  'jremmen/vim-ripgrep',
  'junegunn/fzf',
  'junegunn/fzf.vim',
  'vimwiki/vimwiki',
  'sheerun/vim-polyglot',
  {
    'nvim-treesitter/nvim-treesitter',
    cmd = "TSUpdateSync"
  },
  'nvim-treesitter/playground',
  'nvim-treesitter/nvim-treesitter-context',
  {
    'tbabej/taskwiki',
    cond = function()
      local output = vim.fn.has("unix")
      local termuxDirExists = vim.fn.isdirectory("/data/data/com.termux/")
      return {output == 1 and not termuxDirExists}
    end
  },
  {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v2.x',
    dependencies = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},             -- Required
      {'williamboman/mason.nvim'},           -- Optional
      {'williamboman/mason-lspconfig.nvim'}, -- Optional

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},     -- Required
      {'hrsh7th/cmp-nvim-lsp'}, -- Required
      {'L3MON4D3/LuaSnip'},     -- Required
    }
  }
})

--  {'theprimeagen/harpoon'},
--  {'theprimeagen/refactoring.nvim'},
--  {'mbbill/undotree'},
--  {'tpope/vim-fugitive'},
