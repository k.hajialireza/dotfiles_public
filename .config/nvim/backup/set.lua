vim.opt.compatible = false -- [ ] if statement for different environments

vim.opt.errorbells = false	-- :set noeb

--vim.opt.guicursor = ""

vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.number = true -- :set nu
vim.opt.relativenumber = true -- :set rnu

--vim.opt.tabstop = 4
--vim.opt.softtabstop = 4
--vim.opt.shiftwidth = 4
--vim.opt.expandtab = true

vim.opt.smartindent = true -- :set si

--vim.opt.wrap = false

--vim.opt.swapfile = false
--vim.opt.backup = false
--vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
--vim.opt.undofile = true

vim.opt.hlsearch = true -- :set hls
vim.opt.incsearch = true -- :set is

--vim.opt.termguicolors = true

--vim.opt.scrolloff = 8
--vim.opt.signcolumn = "yes"
--vim.opt.isfname:append("@-@")

--vim.opt.updatetime = 50

--vim.opt.colorcolumn = "80"

